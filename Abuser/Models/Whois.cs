﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abuser.Models
{
    public class Whois
    {
        public int ID { get; set; }

        public bool Registered { get; set; }
        public string Disclaimer { get; set; }
        public string RegistryDomainId { get; set; }
        public string RegistrarIanaId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateExpires { get; set; }
        public DateTime DateUpdated { get; set; }
        public string DomainName { get; set; }
        public string ScreenshotId { get; set; }
        public string NameServers { get; set; }
        public string Contacts { get; set; }
        public string Emails { get; set; }
        public string Statuses { get; set; }
    }
}
