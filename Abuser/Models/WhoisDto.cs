﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abuser.Models
{
    public class WhoisDto
    {
        [JsonProperty("registered")]
        public bool Registered { get; set; }
        [JsonProperty("disclaimer")]
        public string Disclaimer { get; set; }
        [JsonProperty("registry_domain_id")]
        public string RegistryDomainId { get; set; }
        [JsonProperty("registrar_iana_id")]
        public string RegistrarIanaId { get; set; }
        [JsonProperty("date_created")]
        public DateTime DateCreated { get; set; }
        [JsonProperty("date_expires")]
        public DateTime DateExpires { get; set; }
        [JsonProperty("date_updated")]
        public DateTime DateUpdated { get; set; }
        [JsonProperty("domain_status")]
        public string[] DomainStatus { get; set; }
        [JsonProperty("nameservers")]
        public string[] NameServers { get; set; }
        //[JsonProperty("emails")]
        //public string[] Emails { get; set; }
        [JsonProperty("contacts")]
        public WhoisContactDto[] Contacts { get; set; }
        [JsonProperty("domain_name")]
        public string DomainName { get; set; }
        [JsonProperty("_cached")]
        public bool Cached { get; set; }
        [JsonProperty("_cached_datetime")]
        public DateTime CachedDateTime { get; set; }
    }
}
