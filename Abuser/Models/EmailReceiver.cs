﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Abuser.Models
{
    public class EmailReceiver
    {
        public int ID { get; set; }

        [Required(ErrorMessage = "Please enter an email")]
        [EmailAddress(ErrorMessage = "Please specify correct email address")]
        public string Email { get; set; }
    }
}
