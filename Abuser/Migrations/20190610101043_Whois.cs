﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Abuser.Migrations
{
    public partial class Whois : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Whoises",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Contacts = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DateExpires = table.Column<DateTime>(nullable: false),
                    DateUpdated = table.Column<DateTime>(nullable: false),
                    Disclaimer = table.Column<string>(nullable: true),
                    DomainName = table.Column<string>(nullable: true),
                    Emails = table.Column<string>(nullable: true),
                    NameServers = table.Column<string>(nullable: true),
                    Registered = table.Column<bool>(nullable: false),
                    RegistrarIanaId = table.Column<string>(nullable: true),
                    RegistryDomainId = table.Column<string>(nullable: true),
                    ScreenshotId = table.Column<string>(nullable: true),
                    Statuses = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Whoises", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Whoises");
        }
    }
}
