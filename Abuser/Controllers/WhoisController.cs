﻿using Abuser.Data;
using Abuser.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abuser.Controllers
{
    [Authorize]
    public class WhoisController : Controller
    {
        private IWhoisRepository repository;
        private IWhoisService whoisService;

        public WhoisController(IWhoisRepository repo, IWhoisService whois)
        {
            repository = repo;
            whoisService = whois;
        }

        public ViewResult Index()
        {
            return View(repository.Whoises);
        }

        public async Task<RedirectToActionResult> PostFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
            {
                return RedirectToAction(nameof(Index));
            }
            var path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot",
                   file.FileName);
            var lines = new List<string>();
            using (var stream = file.OpenReadStream())
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                    string line = null;
                    while((line = await reader.ReadLineAsync()) != null)
                    {
                        lines.Add(line);
                    }
                }
            }

            foreach (var line in lines)
            {
                await whoisService.ProcessUrl(line);
            }

            return RedirectToAction(nameof(Index));
        }

        public IActionResult Screenshot(string id)
        {
            if (!System.IO.File.Exists(@"Screenshots\" + id + ".pdf"))
            {
                return NotFound();
            }
            byte[] file = System.IO.File.ReadAllBytes(@"Screenshots\" + id + ".pdf");
            return File(file, "application/pdf");
        }
    }
}
