﻿using Abuser.Core.Services;
using Abuser.Data;
using Abuser.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PuppeteerSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abuser.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private IEmailReceiversRepository repository;

        public HomeController(IEmailReceiversRepository repo)
        {
            repository = repo;
        }

        public ViewResult Index()
        {
            return View(repository.EmailReceivers);
        }

        public ViewResult Create() => View("Edit", new EmailReceiver());

        public ViewResult Edit(int id) =>
            View(repository.EmailReceivers
                .FirstOrDefault(r => r.ID == id));

        [HttpPost]
        public IActionResult Edit(EmailReceiver receiver)
        {
            if (ModelState.IsValid)
            {
                repository.SaveEmailReceiver(receiver);
                TempData["message"] = $"{receiver.Email} has been saved";
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return View(receiver);
            }
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            EmailReceiver deletedReceiver = repository.DeleteEmailReceiver(id);
            if (deletedReceiver != null)
            {
                TempData["message"] = $"{deletedReceiver.Email} was deleted";
            }
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> FillForm()
        {
            var fishingPage = "https://safebrowsing.google.com/safebrowsing/report_phish/?hl=en";
            await new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision);
            using (var browser = await Puppeteer.LaunchAsync(new LaunchOptions { Headless = true }))
            using (var page = await browser.NewPageAsync())
            {
                await page.GoToAsync(fishingPage, WaitUntilNavigation.Networkidle0);
                await page.WaitForSelectorAsync("#formprincipal");
                await page.FocusAsync("#url");
                await page.Keyboard.TypeAsync("Hello World");

                await page.WaitForSelectorAsync(".g-recaptcha");
                var recaptchaSitekey = await page.EvaluateFunctionAsync<string>(@"() => {
                    var sitekey = document.querySelector('.g-recaptcha').getAttribute('data-sitekey');
                    return sitekey;}");

                var context = new ReCaptchaContext(recaptchaSitekey, "https://safebrowsing.google.com");
                await context.Handle();
                var token = context.Token;
                await page.EvaluateFunctionAsync(@"() => { document.getElementById('g-recaptcha-response').style.display = 'block'}");
                await page.FocusAsync("#g-recaptcha-response");
                await page.Keyboard.TypeAsync(token);

                var element = await page.QuerySelectorAsync("#formprincipal");
                var image = await element.ScreenshotDataAsync();

                // submmit form
                var submitButtons = await page.QuerySelectorAllAsync("input[name=submit]");
                await submitButtons[0].ClickAsync();

                return File(image, "image/png");
            }
        }
    }
}
