﻿using Abuser.Data;
using Abuser.Models;
using Newtonsoft.Json;
using PuppeteerSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Abuser.Services
{
    public class WhoisService : IWhoisService
    {
        static readonly string WHOSISAPI_URL = "http://api.whoapi.com/?apikey=a3cb0f2d16796b8040b626df30dc09a3&r=whois";
        static readonly string SMTP2GO_URL = "https://api.smtp2go.com/v3/";
        static readonly string SMTP2GO_APIKEY = "api-9D437600757A11E98F62F23C91BBF4A0";
        private readonly IWhoisRepository whoisRepository;
        private readonly IEmailReceiversRepository emailReceiversRepository;
        private static readonly object syncRoot = new object();

        public WhoisService(IWhoisRepository whoisRepo, IEmailReceiversRepository emailReceiversRepo)
        {
            whoisRepository = whoisRepo;
            emailReceiversRepository = emailReceiversRepo;       
        }

        public async Task ProcessUrl(string url)
        {
            var screenshotId = await TakeScreenShot(url);
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(WHOSISAPI_URL + $"&domain={url}");
                if (response.IsSuccessStatusCode)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    var whois = JsonConvert.DeserializeObject<WhoisDto>(body);

                    whoisRepository.SaveWhois(new Whois
                    {
                        NameServers = JsonConvert.SerializeObject(whois.NameServers),
                        Contacts = JsonConvert.SerializeObject(whois.Contacts),
                        //Emails = JsonConvert.SerializeObject(whois.Emails),
                        Statuses = JsonConvert.SerializeObject(whois.DomainStatus),
                        ScreenshotId = screenshotId,
                        DateCreated = whois.DateCreated,
                        DateExpires = whois.DateExpires,
                        DateUpdated = whois.DateUpdated,
                        Disclaimer = whois.Disclaimer,
                        DomainName = whois.DomainName,
                        Registered = whois.Registered,
                        RegistrarIanaId = whois.RegistrarIanaId,
                        RegistryDomainId = whois.RegistryDomainId
                    });

                    await SendEmailWithAttachment(screenshotId);
                }
            }
        }

        private static async Task<string> TakeScreenShot(string line)
        {

            if (!line.StartsWith("http://") && !line.StartsWith("https://"))
            {
                line = $"http://{line}";
            }
            Directory.CreateDirectory("Screenshots");

            var filename = Guid.NewGuid().ToString();
            await new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision);
            var browser = await Puppeteer.LaunchAsync(new LaunchOptions
            {
                Headless = true,
                Args = new string[] { "--no-sandbox", "--disable-setuid-sandbox" }
            });
            var page = await browser.NewPageAsync();
            await page.GoToAsync(line, WaitUntilNavigation.Networkidle2);
            await page.SetViewportAsync(new ViewPortOptions { Width = 1280, Height = 800 });

            await page.PdfAsync(@"Screenshots\" + filename + ".pdf");
            await browser.CloseAsync();

            return filename;
        }

        private async Task SendEmailWithAttachment(string screenshotId)
        {
            var emailReceivers = emailReceiversRepository.EmailReceivers
                .Select(r => r.Email).ToList();
            var base64 = GetBase64String(screenshotId);
            var requestObj = new
            {
                api_key = SMTP2GO_APIKEY,
                to = emailReceivers, //new List<string> { /*"julien.sorlin.2017@gmail.com", "contact@frenchwebmarketing.club"*/
                    //"tention@bigmir.net" },
                sender = "scan@thephishingmonitor.com",
                subject = "Screenshot",
                html_body = "<h1>See an attachment</h1>",
                attachments = new List<object> {
                    new {
                        filename = "screenshot.pdf",
                        fileblob = base64,
                        mimetype = "application/pdf"
                    }
                }
            };
            using (var client = new HttpClient())
            using (var request = new HttpRequestMessage(HttpMethod.Post, SMTP2GO_URL + "email/send"))
            {
                var json = JsonConvert.SerializeObject(requestObj);
                using (var stringContent = new StringContent(json, Encoding.UTF8, "application/json"))
                {
                    request.Content = stringContent;
                    using (var response = await client.SendAsync(request).ConfigureAwait(false))
                    {
                        var responseContent = await response.Content.ReadAsStringAsync();
                    }
                }
            }
        }

        private static string GetBase64String(string screenshotId)
        {
            byte[] pdfBytes = File.ReadAllBytes(@"Screenshots\" + screenshotId + ".pdf");
            return Convert.ToBase64String(pdfBytes);
        }
    }
}
