﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abuser.Services
{
    public interface IWhoisService
    {
        Task ProcessUrl(string url);
    }
}
