﻿using Abuser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abuser.Data
{
    public class EFWhoisRepository : IWhoisRepository
    {
        private ApplicationDbContext context;

        public EFWhoisRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<Whois> Whoises => context.Whoises;

        public Whois DeleteWhois(int id)
        {
            Whois dbEntry = context.Whoises
                .FirstOrDefault(w => w.ID == id);
            if (dbEntry != null)
            {
                context.Whoises.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

        public void SaveWhois(Whois whois)
        {
            if (whois.ID == 0)
            {
                context.Whoises.Add(whois);
            }
            else
            {
                Whois dbEntry = context.Whoises
                    .FirstOrDefault(w => w.ID == whois.ID);
                if (dbEntry != null)
                {
                    dbEntry.Contacts = whois.Contacts;
                    dbEntry.DateCreated = whois.DateCreated;
                    dbEntry.DateExpires = whois.DateExpires;
                    dbEntry.DateUpdated = whois.DateUpdated;
                    dbEntry.Disclaimer = whois.Disclaimer;
                    dbEntry.DomainName = whois.DomainName;
                    dbEntry.Emails = whois.Emails;
                    dbEntry.NameServers = whois.NameServers;
                    dbEntry.Registered = whois.Registered;
                    dbEntry.RegistrarIanaId = whois.RegistrarIanaId;
                    dbEntry.RegistryDomainId = whois.RegistryDomainId;
                    dbEntry.ScreenshotId = whois.ScreenshotId;
                    dbEntry.Statuses = whois.Statuses;
                }
            }
            context.SaveChanges();
        }
    }
}
