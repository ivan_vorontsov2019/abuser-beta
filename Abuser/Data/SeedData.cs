﻿using Abuser.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abuser.Data
{
    public class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            ApplicationDbContext context = app.ApplicationServices
                .GetRequiredService<ApplicationDbContext>();
            context.Database.Migrate();
            if (!context.EmailReceivers.Any()) {
                context.AddRange(
                    new EmailReceiver
                    {
                        Email = "julien.sorlin.2017@gmail.com"
                    },
                    new EmailReceiver
                    {
                        Email = "contact@frenchwebmarketing.club"
                    });
                context.SaveChanges();
            }  
        }
    }
}
