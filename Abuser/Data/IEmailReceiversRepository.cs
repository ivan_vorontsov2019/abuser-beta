﻿using Abuser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abuser.Data
{
    public interface IEmailReceiversRepository
    {
        IQueryable<EmailReceiver> EmailReceivers { get; }
        void SaveEmailReceiver(EmailReceiver emailReceiver);
        EmailReceiver DeleteEmailReceiver(int id);
    }
}
