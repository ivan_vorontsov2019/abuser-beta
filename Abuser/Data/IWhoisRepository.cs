﻿using Abuser.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Abuser.Data
{
    public interface IWhoisRepository
    {
        IQueryable<Whois> Whoises { get; }
        void SaveWhois(Whois whois);
        Whois DeleteWhois(int id);
    }
}
