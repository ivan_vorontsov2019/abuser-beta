﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abuser.Models;

namespace Abuser.Data
{
    public class EFEmailReceiversRepository : IEmailReceiversRepository
    {
        private ApplicationDbContext context;

        public EFEmailReceiversRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public IQueryable<EmailReceiver> EmailReceivers => context.EmailReceivers;

        public EmailReceiver DeleteEmailReceiver(int id)
        {
            EmailReceiver dbEntry = context.EmailReceivers
                .FirstOrDefault(r => r.ID == id);
            if (dbEntry != null)
            {
                context.EmailReceivers.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }

        public void SaveEmailReceiver(EmailReceiver emailReceiver)
        {
            if (emailReceiver.ID == 0)
            {
                context.EmailReceivers.Add(emailReceiver);
            }
            else
            {
                EmailReceiver dbEntry = context.EmailReceivers
                    .FirstOrDefault(r => r.ID == emailReceiver.ID);
                if (dbEntry != null)
                {
                    dbEntry.Email = emailReceiver.Email;
                }
            }
            context.SaveChanges();
        }
    }
}
