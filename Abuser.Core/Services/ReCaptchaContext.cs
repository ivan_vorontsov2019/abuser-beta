﻿using Abuser.Core.Services.ReCaptcha;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Abuser.Core.Services
{
    public class ReCaptchaContext
    {
        private State _state;
        private string dataSitekey;
        private string captchaId;
        private string pageUrl;
        private string token;

        public ReCaptchaContext(string dataSitekey, string url)
        {
            this.dataSitekey = dataSitekey;
            this.pageUrl = url;
            this.State = new PostCaptchaState(this);
        }

        public State State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string DataSiteKey
        {
            get { return dataSitekey; }
        }

        public string CaptchaId
        {
            get { return captchaId; }
            set { captchaId = value; }
        }

        public string Token
        {
            get { return token; }
            set { token = value; }
        }

        public string PageUrl
        {
            get { return pageUrl; }
        }

        public async Task Handle()
        {
            await this.State.Handle();
        }

        public static void Test(string[] args)
        {

        }
    }
}
