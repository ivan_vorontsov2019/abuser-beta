﻿using Abuser.Core.Services.NormalCaptcha;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Abuser.Core.Services

{
    public class NormalCaptchaContext
    {
        private State _state;
        private byte[] img;
        private string capchaId;
        private string capchaText;

        public NormalCaptchaContext(byte[] image)
        {
            this.img = image;
            this._state = new PostCapchaState(this);
        }

        public State State
        {
            get { return this._state; }
            set { this._state = value; }
        }

        public byte[] Image
        {
            get { return img; }
        }

        public string CapchaId
        {
            get { return capchaId; }
            set { capchaId = value; }
        }

        public string CapchaText
        {
            get { return capchaText; }
            set { capchaText = value; }
        }

        public async Task Handle()
        {
            await this.State.Handle();
        }

        public static void Test(string[] args)
        {
            string address = "https://confluence.atlassian.com/conf65/files/939701877/939701879/1/1509520547748/captcha.png";
            HttpClient client = new HttpClient();
            byte[] img;

            client.GetAsync(address).ContinueWith(
                (requestTask) =>
                {
                    HttpResponseMessage response = requestTask.Result;

                    response.EnsureSuccessStatusCode();

                    MemoryStream ms = new MemoryStream();

                    response.Content.CopyToAsync(ms).ContinueWith(
                        async (copyTask) =>
                        {
                            img = ms.ToArray();
                            ms.Close();
                            var capcha = new NormalCaptchaContext(img);
                            await capcha.Handle();
                        });
                });

            Console.WriteLine("Hit Enter to exit...");
            Console.ReadLine();
        }
    }
}
