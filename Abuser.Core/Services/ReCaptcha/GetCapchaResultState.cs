﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Abuser.Core.Services.ReCaptcha
{
    public class GetCapchaResultState : State
    {
        private string baseUrl = "http://2captcha.com/";
        private string apiKey = "42e2b726c8db5f65606862b45af990e9";

        public GetCapchaResultState(ReCaptchaContext context)
        {
            Context = context;
        }

        public override async Task Handle()
        {
            var random = new Random();
            await Task.Delay(TimeSpan.FromSeconds(random.NextDouble() * 5 + 15));
            string result;
            using (var client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync($"{baseUrl}res.php?key={apiKey}&action=get&id={Context.CaptchaId}");
                result = await response.Content.ReadAsStringAsync();
            }
            while(result == "CAPCHA_NOT_READY")
            {
                await Task.Delay(TimeSpan.FromSeconds(5));
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.GetAsync($"{baseUrl}res.php?key={apiKey}&action=get&id={Context.CaptchaId}");
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            Context.Token = result.Split('|')[1];
        }
    }
}
