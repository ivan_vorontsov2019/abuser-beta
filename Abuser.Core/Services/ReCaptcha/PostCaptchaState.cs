﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Abuser.Core.Services.ReCaptcha
{
    public class PostCaptchaState : State
    {
        private string baseUrl = "http://2captcha.com/";
        private string apiKey = "42e2b726c8db5f65606862b45af990e9";

        public PostCaptchaState(ReCaptchaContext context)
        {
            this.Context = context;
        }

        public override async Task Handle()
        {
            using (var client = new HttpClient())
            {
                using (var form = new MultipartFormDataContent())
                {
                    form.Add(new StringContent("userrecaptcha"), "method");
                    form.Add(new StringContent(apiKey), "key");
                    form.Add(new StringContent(Context.DataSiteKey), "googlekey");
                    form.Add(new StringContent(Context.PageUrl), "pageurl");
                    HttpResponseMessage response = await client.PostAsync(baseUrl + "in.php", form);
                    var result = await response.Content.ReadAsStringAsync();
                    Context.CaptchaId = result.Split('|')[1];
                }
            }
            Context.State = new GetCapchaResultState(Context);
            await Context.Handle();
        }
    }
}
