﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Abuser.Core.Services.ReCaptcha
{
    public abstract class State
    {
        private ReCaptchaContext context;

        public abstract Task Handle();

        public ReCaptchaContext Context
        {
            get { return context; }
            set { context = value; }
        }
    }
}
