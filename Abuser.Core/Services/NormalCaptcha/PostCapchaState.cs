﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Abuser.Core.Services.NormalCaptcha
{
    public class PostCapchaState : State
    {
        private string baseUrl = "http://2captcha.com/";
        private string apiKey = "";

        public PostCapchaState(NormalCaptchaContext context)
        {
            this.context = context;
        }

        public async override Task Handle()
        {
            using (HttpClient client = new HttpClient())
            {
                using (var form = new MultipartFormDataContent())
                {
                    form.Add(new StringContent("post"), "method");
                    form.Add(new StringContent(apiKey), "key");
                    form.Add(new ByteArrayContent(context.Image, 0, context.Image.Length), "file", "captcha.png");
                    HttpResponseMessage response = await client.PostAsync(baseUrl + "in.php", form);
                    context.CapchaId = await response.Content.ReadAsStringAsync();
                }
            }
            this.Context.State = new GetCapchaResultState(this.Context, context.CapchaId);
            await this.Context.Handle();
        }
    }
}
