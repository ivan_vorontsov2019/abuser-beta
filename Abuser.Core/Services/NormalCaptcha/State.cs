﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Abuser.Core.Services.NormalCaptcha
{
    public abstract class State
    {
        protected NormalCaptchaContext context;

        public abstract Task Handle();

        public NormalCaptchaContext Context
        {
            set { context = value; }
            get { return context; }
        }
    }
}
