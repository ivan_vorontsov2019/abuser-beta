﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Abuser.Core.Services.NormalCaptcha
{
    public class GetCapchaResultState : State
    {
        private string capchaId;
        private string baseUrl = "http://2captcha.com/";
        private string apiKey = "";

        public GetCapchaResultState(NormalCaptchaContext manager, string capchaId)
        {
            this.capchaId = capchaId;
            this.context = manager;
        }

        public override async Task Handle()
        {
            await Task.Delay(TimeSpan.FromSeconds(5));
            string result = null;
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync($"{baseUrl}res.php?key={apiKey}&action=get&id={this.capchaId}");
                result = await response.Content.ReadAsStringAsync();
            }
            while (result == "CAPCHA_NOT_READY") {
                await Task.Delay(TimeSpan.FromSeconds(5));
                using (HttpClient client = new HttpClient())
                {
                    HttpResponseMessage response = await client.GetAsync($"{baseUrl}res.php?key={apiKey}&action=get&id={this.capchaId}");
                    result = await response.Content.ReadAsStringAsync();
                }
            }
            this.context.CapchaText = result;
        }
    }
}
